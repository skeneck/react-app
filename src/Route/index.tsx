import React from 'react';
import Dashboard from '../Pages/ContentsToMenuItem/Dashboard/Dashboard';
import Avatars from '../Pages/ContentsToMenuItem/Avatars/Avatars';
import Buttons from '../Pages/ContentsToMenuItem/ButtonsType/Buttons';
import Panels from '../Pages/ContentsToMenuItem/Panels/Panels';
import BasicForm from '../Pages/ContentsToMenuItem/BasicForm/BasicForm';
import BasicTable from '../Pages/ContentsToMenuItem/BasicTable/BasicTable';
import DataTables from '../Pages/ContentsToMenuItem/DataTables/DataTables';
import Login from '../Pages/Connection/Login';
import Profil from '../Pages/Profil/Profil';


export interface Meta {
	pageTitle: string;
	breadcrumb: Array<{text: string}>;
}

export interface Route {
	path: string;
	component: React.ComponentType;
	meta: Meta;
}

type R = Array<Route>

const routes: R = [
	{
		path: "/",
		component: Dashboard,
		meta: {
			pageTitle: "Dashboard",
			breadcrumb: [
				{
					text: "",
					// active: false,
				},
			],
		},
	},
	{
		path: "/login",
		component: Login,
		meta: {
			pageTitle: "",
			breadcrumb: [
				{
					text: "",
					// active: false,
				},
			],
		},
	},
	{
		path: "/profil",
		component: Profil,
		meta: {
			pageTitle: "Profil",
			breadcrumb: [
				{
					text: "",
					// active: false,
				},
			],
		},
	},
	{
		path: "/bases-avatars",
		component: Avatars,
		meta: {
			pageTitle: "Avatars",
			breadcrumb: [
				{
					text: "Bases",
					// active: false,
				},
			],
		},
	},
	{
		path: "/bases-buttons",
		component: Buttons,
		meta: {
			pageTitle: "Buttons",
			breadcrumb: [
				{
					text: "Bases",
					// active: false,
				},
			],
		},
	},
	{
		path: "/bases-panels",
		component: Panels,
		meta: {
			pageTitle: "Panels",
			breadcrumb: [
				{
					text: "Bases",
					// active: false,
				},
			],
		},
	},
	{
		path: "/forms-basic-form",
		component: BasicForm,
		meta: {
			pageTitle: "Basic-Form",
			breadcrumb: [
				{
					text: "Forms",
					// active: false,
				},
			],
		},
	},
	{
		path: "/tables-basic-table",
		component: BasicTable,
		meta: {
			pageTitle: "Basic-Table",
			breadcrumb: [
				{
					text: "Tables",
					// active: false,
				},
			],
		},
	},
	{
		path: "/tables-datatables",
		component: DataTables,
		meta: {
			pageTitle: "Datatables",
			breadcrumb: [
				{
					text: "Tables",
					// active: false,
				},
			],
		},
	},
];

export default routes;
