import { BrowserRouter as Router } from "react-router-dom";
import Headers from "./Pages/Headers/Headers";
import SideBars from "./Pages/SideBars/SideBars";
import ContentsToMenuItem from "./Pages/ContentsToMenuItem/ContentsToMenuItem";
import Footer from "./Pages/Footers/Footers";
// import { selectTest1, selectTest2, increment, decrement, setBooleanTrue, setBooleanFalse } from './Store/slices'
// import { useAppDispatch, useAppSelector } from './Store/hooks'


// SCRIPT START
export default function App () {
	return (
		<div className="wrapper">
			<Router>
				<Headers />
				<SideBars />
				<div className="main-panel">
					<ContentsToMenuItem />
					<Footer />
				</div>
			</Router>
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
