import Breadcrumb from '../Breadcrumb/Breadcrumb';


// SCRIPT START
export default function DataTables() {
	return (
		<div className="page-inner">
			<Breadcrumb pathName={ window.location.pathname } />
			<div className="row">
				<div>Page DataTables !</div>
			</div>
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
