import { CSSProperties, FC } from "react";
import { Routes, Route, useLocation, Outlet } from "react-router-dom";
import { AnimatePresence, motion } from "framer-motion";
import routes from '../../Route/index';


// SCRIPT START
export const Wrapper: FC = () => {
	return (
		<motion.div
			initial="initial"
			animate="in"
			exit="out"
			variants={{
				initial: {
					opacity: 0
				},
				in: {
					opacity: 1
				},
				out: {
					opacity: 0
				}
			}}
			transition={{
				type: "spring",
				damping: 10,
				stiffness: 50
			}}
		>
			<Outlet />
		</motion.div>
	);
};

export default function ContentsToMenuItem() {
	let location = useLocation();

	return (
		<div className="content">
			<div className="page-inner">
				<AnimatePresence exitBeforeEnter>
					<Routes location={ location } key={ location.pathname }>
						<Route element={ <Wrapper /> }>
							{ routes.map((route, index) => (
								<Route 
									key={ index } 
									path={ route.path }  
									element={ <route.component /> }
								/>
							)) }
						</Route>
					</Routes>
				</AnimatePresence>
			</div>
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
