import Breadcrumb from '../Breadcrumb/Breadcrumb';


// SCRIPT START
export default function Panels() {
	return (
		<div className="page-inner">
			<Breadcrumb pathName={ window.location.pathname } />
			<div className="row">
				<div>Page Panels !</div>
			</div>
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END

