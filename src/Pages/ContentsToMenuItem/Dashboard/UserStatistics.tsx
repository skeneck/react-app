// SCRIPT START
export default function UserStatistics(props: {title: string}) {
	return (
		<div className="col-md-8">
			<div className="card">
				<div className="card-header">
					<div className="card-head-row">
						<div className="card-title">{ props.title }</div>
					</div>
				</div>
				<div className="card-body">
					<div className="chart-container" style={{ minHeight: "375px" }}>
						<div className="chartjs-size-monitor" style={{ position: "absolute", inset: "0px", overflow: "hidden", pointerEvents: "none", visibility: "hidden", zIndex: "-1" }}>
							<div className="chartjs-size-monitor-expand" style={{ position: "absolute", left: "0", top: "0", right: "0", bottom: "0", overflow: "hidden", pointerEvents: "none", visibility: "hidden", zIndex: "-1" }}>
								<div style={{ position: "absolute", width: "1000000px", height: "1000000px", left: "0", top: "0" }}></div>
							</div>
							<div className="chartjs-size-monitor-shrink" style={{ position: "absolute", left: "0", top: "0", right: "0", bottom: "0", overflow: "hidden", pointerEvents: "none", visibility: "hidden", zIndex: "-1" }}>
								<div style={{ position: "absolute", width:"200%", height:"200%", left: "0", top: "0" }}></div>
							</div>
						</div>
						<canvas id="statisticsChart" width="641" height="375" style={{ display: "block", width: "641px", height: "375px" }} className="chartjs-render-monitor"></canvas>
					</div>
					<div id="myChartLegend">
						<ul className="0-legend html-legend">
							<li>
								<span style={{ backgroundColor: "#f3545d" }}></span>Subscribers
							</li>
							<li>
								<span style={{ backgroundColor: "#fdaf4b" }}></span>New Visitors
							</li>
							<li>
								<span style={{ backgroundColor: "#177dff" }}></span>Active Users
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
