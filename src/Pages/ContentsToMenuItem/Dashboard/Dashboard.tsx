import Breadcrumb from '../Breadcrumb/Breadcrumb';
import { selectNewUserSlice, changeNewUser } from '../../../Store/slices';
import { useAppSelector, useAppDispatch } from '../../../Store/hooks';
import OverallAndIncomeAndSpendStatistics from './OverallAndIncomeAndSpendStatistics';
import UserStatistics from './UserStatistics';


// SCRIPT START
interface Circles {
	wrp: {};
	text: {};
	chartjs:{};
}

export default function Dashboard() {
	const newUser = useAppSelector(selectNewUserSlice)
	const dispatch = useAppDispatch()

	return (
		<div className="page-inner">
			<Breadcrumb pathName={ window.location.pathname } />
			<div className="row">
				<OverallAndIncomeAndSpendStatistics title="Overall statistics" category="Daily information about statistics in system">
					<div className="d-flex flex-wrap justify-content-around pb-2 pt-4">
						<div className="px-2 pb-2 pb-md-0 text-center">
							<div id="circles-1" style={{ cursor: "pointer" }}>
								<div className="circles-wrp" style={ circles.wrp }>
									<svg xmlns="http://www.w3.org/2000/svg" width="90" height="90">
										<path fill="transparent" stroke="#f1f1f1" stroke-width="7" d="M 44.99154756204665 3.500000860767564 A 41.5 41.5 0 1 1 44.942357332570026 3.500040032273624 Z" className="circles-maxValueStroke"></path>
										<path fill="transparent" stroke="#FF9E27" stroke-width="7" d="M 44.99154756204665 3.500000860767564 A 41.5 41.5 0 1 1 20.644357636259837 78.60137921350231 " className="circles-valueStroke"></path>
									</svg>
									<div className="circles-text" style={ circles.text } onClick={ () => dispatch(changeNewUser()) }>{ newUser }</div>
								</div>
							</div>
							<h6 className="fw-bold mt-3 mb-0">New Users</h6>
						</div>    
						<div className="px-2 pb-2 pb-md-0 text-center">
							<div id="circles-2">
								<div className="circles-wrp" style={ circles.wrp }>
									<svg xmlns="http://www.w3.org/2000/svg" width="90" height="90">
										<path fill="transparent" stroke="#f1f1f1" stroke-width="7" d="M 44.99154756204665 3.500000860767564 A 41.5 41.5 0 1 1 44.942357332570026 3.500040032273624 Z" className="circles-maxValueStroke"></path>
										<path fill="transparent" stroke="#2BB930" stroke-width="7" d="M 44.99154756204665 3.500000860767564 A 41.5 41.5 0 1 1 5.5495771787290025 57.88076625138973 " className="circles-valueStroke"></path>
									</svg>
									<div className="circles-text" style={ circles.text }>36</div>
								</div>
							</div>
							<h6 className="fw-bold mt-3 mb-0">Sales</h6>
						</div>
						<div className="px-2 pb-2 pb-md-0 text-center">
							<div id="circles-3">
								<div className="circles-wrp" style={ circles.wrp }>
									<svg xmlns="http://www.w3.org/2000/svg" width="90" height="90">
										<path fill="transparent" stroke="#f1f1f1" stroke-width="7" d="M 44.99154756204665 3.500000860767564 A 41.5 41.5 0 1 1 44.942357332570026 3.500040032273624 Z" className="circles-maxValueStroke"></path>
										<path fill="transparent" stroke="#F25961" stroke-width="7" d="M 44.99154756204665 3.500000860767564 A 41.5 41.5 0 0 1 69.44267714510887 78.53812060894248 " className="circles-valueStroke"></path>
									</svg>
									<div className="circles-text" style={ circles.text }>12</div>
								</div>
							</div>
							<h6 className="fw-bold mt-3 mb-0">Subscribers</h6>
						</div>
					</div>
				</OverallAndIncomeAndSpendStatistics>

				<OverallAndIncomeAndSpendStatistics title="Total income & spend statistics">
					<div className="row py-3">
						<div className="col-md-4 d-flex flex-column justify-content-around">
							<div>
								<h6 className="fw-bold text-uppercase text-success op-8">Total Income</h6>
								<h3 className="fw-bold">$9.782</h3>
							</div>
							<div>
								<h6 className="fw-bold text-uppercase text-danger op-8">Total Spend</h6>
								<h3 className="fw-bold">$1,248</h3>
							</div>
						</div>
						<div className="col-md-8">
							<div id="chart-container">
								<div className="chartjs-size-monitor" style={{ position: "absolute", inset: "0px", overflow: "hidden", pointerEvents: "none", visibility: "hidden", zIndex: "-1" }}>
									<div className="chartjs-size-monitor-expand" style={ circles.chartjs }>
										<div style={{ position: "absolute", width: "1000000px", height: "1000000px", left: "0", top: "0" }}></div>
									</div>
									<div className="chartjs-size-monitor-shrink" style={ circles.chartjs }>
										<div style={{ position: "absolute", width: "200%", height: "200%", left: "0", top: "0" }}></div>
									</div>
								</div>
								<canvas id="totalIncomeChart" width="299" height="150" style={{ display: "block", width: "299px", height: "150px" }} className="chartjs-render-monitor"></canvas>
							</div>
						</div>
					</div>
				</OverallAndIncomeAndSpendStatistics>
			</div>
			<div className='row'>
				<UserStatistics title="User Statistics" />
			</div>
		</div>
	);
}
// SCRIPT END


// STYLE START
const circles: Circles = {
	wrp: {
		position: "relative",
		display: "inline-block"
	},
	text: {
		position: "absolute",
		top: "0px",
		left: "0px",
		textAlign: "center",
		width: "100%",
		fontSize: "31.5px",
		height: "90px",
		lineHeight: "90px"
	},
	chartjs: {
		position: "absolute",
		left: "0",
		top: "0",
		right: "0",
		bottom: "0",
		overflow: "hidden",
		pointerEvents: "none",
		visibility: "hidden",
		zIndex: "-1"
	}
};
// STYLE END
