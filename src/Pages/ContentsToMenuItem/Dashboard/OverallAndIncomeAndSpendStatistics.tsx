// SCRIPT START
interface Props {
	title: string;
	category?: string;
	children: any;
}

export default function OverallAndIncomeAndSpendStatistics(props: Props) {
	return (
		<div className="col-md-6">
			<div className="card full-height">
				<div className="card-body">
					<div className="card-title">{ props.title }</div>
					<div className="card-category" hidden={ props.category?false:true }>{ props.category }</div>
					{ props.children }
				</div>
			</div>
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
