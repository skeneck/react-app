import React from 'react';
import { Link } from "react-router-dom";
import routes, { Route, Meta } from '../../../Route/index';
import './Breadcrumb.css';


interface Props {
	pathName: string;
}

interface State {
	infos: Meta;
}

// SCRIPT START
export default class Breadcrumb extends React.Component<Props, State> {
	constructor(props: any) {
		super(props)
		
		this.state = {
			infos: {
				pageTitle: "",
				breadcrumb: [ {text: ""} ]
			},
		};
	}

	componentDidMount() {
		const route: Route | undefined = routes.find(({ path }) => path === this.props.pathName)

		if (route) {
			this.setState({
				infos: route.meta,
			});
		}
	}
	
	reloadPage() {
		window.location.reload()
	}

	render() {
		return (
			<div className="page-header" hidden={ (this.props.pathName === '/login') }>
				<h4 className="page-title">{ this.state.infos.pageTitle }</h4>
				<ul className="breadcrumbs">
					<li className="nav-home">
						<Link to="/">
							<i className="flaticon-home"></i>
						</Link>
					</li>
					<span hidden={ (this.state.infos.pageTitle === 'Profil') || (this.state.infos.pageTitle === 'Dashboard') }>
						<li className="separator">
							<i className="flaticon-right-arrow"></i>
						</li>
						<li className="nav-item">
							<span>{ this.state.infos.breadcrumb[0].text }</span>
						</li>
					</span>
					<li className="separator">
						<i className="flaticon-right-arrow"></i>
					</li>
					<li className="nav-item">
						<a href=" " className="hover-link" style={{ color: "#1572e8" }} onClick={ () => this.reloadPage() }>{ this.state.infos.pageTitle }</a>
					</li>
				</ul>
			</div>
		);
	}
}
// SCRIPT END


// STYLE START

// STYLE END
