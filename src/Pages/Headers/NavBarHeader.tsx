import { Link} from "react-router-dom";


interface Styles {
	container1: {};
	container2: {};
}

// SCRIPT START
export default function NavBarHeaders() {
	return (
		<nav className="navbar navbar-header navbar-expand-lg" data-background-color="blue2">	
			<div className="container-fluid">
				<ul className="navbar-nav topbar-nav ml-md-auto align-items-center">
					<li className="nav-item dropdown hidden-caret">
						<a className="dropdown-toggle profile-pic" data-toggle="dropdown" href=" " aria-expanded="false">
							<div className="avatar-sm">
								<img src="/assets/img/profile3.jpeg" alt=" " className="avatar-img rounded-circle" />
							</div>
						</a>
						<ul className="dropdown-menu dropdown-user animated fadeIn">
							<div className="scroll-wrapper dropdown-user-scroll scrollbar-outer" style={ styles.container1 }>
								<div className="dropdown-user-scroll scrollbar-outer scroll-content" style={ styles.container2 }>
									<li>
										<div className="user-box">
											<div className="avatar-lg">
												<img src="/assets/img/profile3.jpeg" alt="" className="avatar-img rounded" />
											</div>
											<div className="u-text">
												<h4>Kéneck SOVI</h4>
												<p className="text-muted">skeneck@gmail.com</p>
											</div>
										</div>
									</li>
									<li>
										<div className="dropdown-divider"></div>
										{/* to="/profil"  to="/login" */}
										<Link to="#" className="dropdown-item">Mon profil</Link>
										<Link to="#" className="dropdown-item">Déconnexion</Link>
									</li>
								</div>
								<div className="scroll-element scroll-x">
									<div className="scroll-element_outer">
										<div className="scroll-element_size"></div>
										<div className="scroll-element_track"></div>
										<div className="scroll-bar ui-draggable ui-draggable-handle"></div>
									</div>
								</div>
								<div className="scroll-element scroll-y">
									<div className="scroll-element_outer">
										<div className="scroll-element_size"></div>
										<div className="scroll-element_track"></div>
										<div className="scroll-bar ui-draggable ui-draggable-handle"></div>
									</div>
								</div>
							</div>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	);
}
// SCRIPT END


// STYLE START
const styles: Styles = {
	container1: {
		position: "relative"
	},
	container2: {
		height: "auto",
		marginBottom: "0px",
		marginRight: "0px",
		maxHeight: "0px"
	}
};
// STYLE END
