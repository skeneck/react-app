import { Link} from "react-router-dom";


// SCRIPT START
export default function LogoHeaders() {
	return (
		<div className="logo-header" data-background-color="blue">
			<Link to="/" className="logo">
				<img src="/assets/img/logo.svg" alt="navbar brand" className="navbar-brand" />
			</Link>
			<button className="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
				<span className="navbar-toggler-icon">
					<i className="icon-menu"></i>
				</span>
			</button>
			<button className="topbar-toggler more"><i className="icon-options-vertical"></i></button>
			<div className="nav-toggle">
				<button className="btn btn-toggle toggle-sidebar">
					<i className="icon-menu"></i>
				</button>
			</div>
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
