import LogoHeader from "./LogoHeader";
import NavBarHeader from "./NavBarHeader";


// SCRIPT START
export default function Headers() {
	return (
		<div className="main-header">
			{/* Logo Header */}
			<LogoHeader />
			{/* End Logo Header */}

			{/* Navbar Header */}
			<NavBarHeader />
			{/* End Navbar */}
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
