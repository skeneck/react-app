// SCRIPT START
export default function Footers() {
	return (
		<footer className="footer">
			<div className="container-fluid">
				<nav className="pull-left">
					<ul className="nav">
						<li className="nav-item">
							<a className="nav-link" href="https://www.themekita.com" target="_blank" rel="noreferrer">
								ThemeKita
							</a>
						</li>
					</ul>
				</nav>
				<div className="copyright ml-auto">
					2022, faire avec <i className="fa fa-heart heart text-danger"></i> par <a href="https://www.linkedin.com/in/keneck-sovi-atchodji-29551b109/" target="_blank" rel="noreferrer">Kéneck SOVI</a>
				</div>
			</div>
		</footer>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
