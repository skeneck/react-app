import { useState, useEffect } from 'react';
import NavSideBars from "./NavSideBars";


// SCRIPT START
export interface ClassName {
	dashboard: boolean;
	base: boolean;
	forms: boolean;
	tables: boolean;
}

interface Style {
	bases_avatars: boolean;
	bases_buttons: boolean;
	bases_panels: boolean;
	forms_basic_form: boolean;
	tables_basic_table: boolean;
	tables_datatables: boolean;
}

const className: ClassName = {
	dashboard: true,
	base: false,
	forms: false,
	tables: false
}

const style: Style = {
	bases_avatars: false,
	bases_buttons: false,
	bases_panels: false,
	forms_basic_form: false,
	tables_basic_table: false,
	tables_datatables: false
}

function saveDataClassInSessionStorage() {
	sessionStorage.setItem('class_name', JSON.stringify({...className}))
}

function saveDataStyleInSessionStorage() {
	sessionStorage.setItem('style', JSON.stringify({...style}))
}

export default function SideBars() {
	const classData = sessionStorage.getItem('class_name');
	const styleData = sessionStorage.getItem('style');

	const [class_name, enableNavItem] = useState({...className});
	const [STYLE, addStyle] = useState({...style});
	
	const handleClick = () => {
		if ((window.location.pathname.slice(1) === 'bases-avatars') || (window.location.pathname.slice(1) === 'bases-buttons') || (window.location.pathname.slice(1) === 'bases-panels')) {
			if (!className.base) {
				className.dashboard = false
				className.base = true
				className.forms = false
				className.tables = false

				enableNavItem({...className})
				saveDataClassInSessionStorage()
			}

			if (window.location.pathname.slice(1) === 'bases-avatars') {
				if (!style.bases_avatars) {
					style.bases_avatars = true
					style.bases_buttons = false
					style.bases_panels = false
					style.forms_basic_form = false
					style.tables_basic_table = false
					style.tables_datatables = false
					
					addStyle({...style})
					saveDataStyleInSessionStorage()
				}
			} else if (window.location.pathname.slice(1) === 'bases-buttons') {
				if (!style.bases_buttons) {
					style.bases_avatars = false
					style.bases_buttons = true
					style.bases_panels = false
					style.forms_basic_form = false
					style.tables_basic_table = false
					style.tables_datatables = false
					
					addStyle({...style})
					saveDataStyleInSessionStorage()
				}
			} else if (window.location.pathname.slice(1) === 'bases-panels') {
				if (!style.bases_panels) {
					style.bases_avatars = false
					style.bases_buttons = false
					style.bases_panels = true
					style.forms_basic_form = false
					style.tables_basic_table = false
					style.tables_datatables = false
					
					addStyle({...style})
					saveDataStyleInSessionStorage()
				}
			}
		} else if ((window.location.pathname.slice(1) === 'tables-basic-table') || (window.location.pathname.slice(1) === 'tables-datatables')) {
			if (!className.tables) {
				className.dashboard = false
				className.base = false
				className.forms = false
				className.tables = true

				enableNavItem({...className})
				saveDataClassInSessionStorage()
			}

			if (window.location.pathname.slice(1) === 'tables-basic-table') {
				if (!style.tables_basic_table) {
					style.bases_avatars = false
					style.bases_buttons = false
					style.bases_panels = false
					style.forms_basic_form = false
					style.tables_basic_table = true
					style.tables_datatables = false
					
					addStyle({...style})
					saveDataStyleInSessionStorage()
				}
			} else if (window.location.pathname.slice(1) === 'tables-datatables') {
				if (!style.tables_datatables) {
					style.bases_avatars = false
					style.bases_buttons = false
					style.bases_panels = false
					style.forms_basic_form = false
					style.tables_basic_table = false
					style.tables_datatables = true
					
					addStyle({...style})
					saveDataStyleInSessionStorage()
				}
			}
		} else if ((window.location.pathname.slice(1) === 'forms-basic-form')) {
			if (!className.forms) {
				className.dashboard = false
				className.base = false
				className.forms = true
				className.tables = false

				enableNavItem({...className})
				saveDataClassInSessionStorage()
			}

			if (!style.forms_basic_form) {
				style.bases_avatars = false
				style.bases_buttons = false
				style.bases_panels = false
				style.forms_basic_form = true
				style.tables_basic_table = false
				style.tables_datatables = false
				
				addStyle({...style})
				saveDataStyleInSessionStorage()
			}
		} else {
			if (!className.dashboard) {
				className.dashboard = true
				className.base = false
				className.forms = false
				className.tables = false
				
				style.bases_avatars = false
				style.bases_buttons = false
				style.bases_panels = false
				style.forms_basic_form = false
				style.tables_basic_table = false
				style.tables_datatables = false
				
				addStyle({...style})
				enableNavItem({...className})
				saveDataClassInSessionStorage()
				saveDataStyleInSessionStorage()
			}
		}
	};

	useEffect(() => {
		if(classData) {
			enableNavItem(JSON.parse(classData)) 
			className.dashboard = false
		}

		if(styleData) {
			addStyle(JSON.parse(styleData)) 
		}
	}, [classData, styleData]);

	return (
		<div className="sidebar sidebar-style-2">
			<div className="sidebar-wrapper scrollbar scrollbar-inner">
				<div className="sidebar-content">
					<NavSideBars onClick={ () => handleClick() } className={ class_name } style={ STYLE } />
				</div>
			</div>
		</div>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
