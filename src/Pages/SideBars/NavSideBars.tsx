import { Link } from "react-router-dom";


// SCRIPT START
export default function NavSideBars(props: any) {
	return (
		<ul className="nav nav-primary">
			<li className={ "nav-item " + ((props.className?.dashboard)?'active':'') } onClick={ props.onClick }>
				<Link to="/">
					<i className="fas fa-home"></i>
					<p>Dashboard</p>
				</Link>
			</li>
			<li className={ "nav-item " + ((props.className?.base)?'active':'') }>
				<a data-toggle="collapse" href="#base" aria-expanded={(props.className?.base)?'true':'false'}>
					<i className="fas fa-layer-group"></i>
					<p>Base</p>
					<span className="caret"></span>
				</a>
				<div className={ "collapse " + ((props.className?.base)?'show':'') } id="base">
					<ul className="nav nav-collapse">
						<li onClick={ props.onClick }>
							<Link to="/bases-avatars" style={ (props.style?.bases_avatars)?{background: 'rgba(199,199,199,.2)'}:{} }>
								<span className="sub-item">Avatars</span>
							</Link>
						</li>
						<li onClick={ props.onClick }>
							<Link to="/bases-buttons" style={ (props.style?.bases_buttons)?{background: 'rgba(199,199,199,.2)'}:{} }>
								<span className="sub-item">Buttons</span>
							</Link>
						</li>
						<li onClick={ props.onClick }>
							<Link to="/bases-panels" style={ (props.style?.bases_panels)?{background: 'rgba(199,199,199,.2)'}:{} }>
								<span className="sub-item">Panels</span>
							</Link>
						</li>
					</ul>
				</div>
			</li>
			<li className={ "nav-item " + ((props.className?.forms)?'active':'') }>
				<a data-toggle="collapse" href="#forms" aria-expanded={(props.className?.forms)?'true':'false'}>
					<i className="fas fa-pen-square"></i>
					<p>Forms</p>
					<span className="caret"></span>
				</a>
				<div className={ "collapse " + ((props.className?.forms)?'show':'') } id="forms">
					<ul className="nav nav-collapse">
						<li onClick={ props.onClick }>
							<Link to="/forms-basic-form" style={ (props.style?.forms_basic_form)?{background: 'rgba(199,199,199,.2)'}:{} }>
								<span className="sub-item">Basic Form</span>
							</Link>
						</li>
					</ul>
				</div>
			</li>
			<li className={ "nav-item " + ((props.className?.tables)?'active':'') }>
				<a data-toggle="collapse" href="#tables" aria-expanded={(props.className?.tables)?'true':'false'}>
					<i className="fas fa-table"></i>
					<p>Tables</p>
					<span className="caret"></span>
				</a>
				<div className={ "collapse " + ((props.className?.tables)?'show':'') } id="tables">
					<ul className="nav nav-collapse">
						<li onClick={ props.onClick }>
							<Link to="/tables-basic-table" style={ (props.style?.tables_basic_table)?{background: 'rgba(199,199,199,.2)'}:{} }>
								<span className="sub-item">Basic Table</span>
							</Link>
						</li>
						<li onClick={ props.onClick }>
							<Link to="/tables-datatables" style={ (props.style?.tables_datatables)?{background: 'rgba(199,199,199,.2)'}:{} }>
								<span className="sub-item">Datatables</span>
							</Link>
						</li>
					</ul>
				</div>
			</li>
		</ul>
	);
}
// SCRIPT END


// STYLE START

// STYLE END
