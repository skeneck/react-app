import { configureStore } from '@reduxjs/toolkit'
import SLICE from './slices'


export const store = configureStore({
	reducer: {
		newUser: SLICE.newUserSliceReducer,
		// test2: SLICE.test2SliceReducer
	},
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
