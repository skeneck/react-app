import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from './index'


// Define a type for the slice state
interface States<T> {
  value: T;
}

// Define the initial state using that type
const initialState1: States<number> = {value: 5}
const initialState2: States<boolean> = {value: false}

const newUserSlice = createSlice({
	name: 'newUser',
	// `createSlice` will infer the state type from the `initialState` argument
	initialState: initialState1,
	reducers: {
		changeNewUser: (state) => {
			state.value += 1
		},
		// decrement: (state) => {
		// 	state.value -= 1
		// }
	},
})

const test2Slice = createSlice({
	name: 'test2',
	// `createSlice` will infer the state type from the `initialState` argument
	initialState: initialState2,
	reducers: {
		// setBooleanTrue: (state) => {
		// 	state.value = true
		// 	console.log('oui')
		// },
		// setBooleanFalse: (state) => {
		// 	state.value = false
		// }
	},
})

const SLICE = {
	newUserSliceReducer: newUserSlice.reducer,
	// test2SliceReducer: test2Slice.reducer
}

// Export actions to other state
export const { changeNewUser } = newUserSlice.actions
// export const { setBooleanTrue, setBooleanFalse } = test2Slice.actions

// Export value to other state
export const selectNewUserSlice = (state: RootState) => state.newUser.value
// export const selectTest2 = (state: RootState) => state.test2.value

// Export all slice in store
export default SLICE
